module CrudFace
  class TopicsController < CrudFace::ApplicationController

    def search
      @results = CrudData::Topic.where("name LIKE :query", query: "%#{params['q']}%")
    end

    def create
      @topic = CrudData::Topic.new(topic_params)
      @topic.save
    end

    def update
      topic.update(topic_params)
    end

    def destroy
      topic.destroy
      respond_to do |format|
        format.html { redirect_to topics_url, notice: 'topic was successfully destroyed.' }
      end
    end

    private

    def topic
      @topic ||= CrudData::Topic.find(params[:id])
    end
    helper_method :topic

    def topic_params
      params.fetch(:topic, {}).permit(:name, :kind)
    end

  end
end
