CrudFace::Engine.routes.draw do
  root to: 'index#index'

  get '/search', to: 'topics#search'
  resources :topics

end
