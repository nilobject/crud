$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "crud_face/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "crud_face"
  s.version     = CrudFace::VERSION
  s.authors     = ["nilobject"]
  s.email       = ["nilobject@gmail.com"]
  s.homepage    = "http://www.github.com/nil0bject/"
  s.summary     = "An interface for common CRUD methods"
  s.description = "CrudFace provides a user interface for creating, reading, updating and deleting data from a source."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", ">= 5.0.0.rc2", "< 5.1"
  s.add_dependency "haml", ">= 4.0.7", "< 5"

  s.add_development_dependency "sqlite3"
end
