# CrudFace
CrudFace provides a user interface for creating, reading, updating and deleting data from a source.

## Usage
How to use my plugin.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'crud_face'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install crud_face
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
