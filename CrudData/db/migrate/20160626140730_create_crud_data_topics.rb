class CreateCrudDataTopics < ActiveRecord::Migration[5.0]
  def change
    create_table :crud_data_topics do |t|
      t.string :name
      t.string :kind

      t.timestamps
    end
  end
end
