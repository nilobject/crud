# Crud*

## Usage

### Build
This will `bundle`, copy the migrations from 'CrudData' to 'Crudem' and then migrate.
```
./bin/build.sh
```

### Develop
```
./bin/console.sh
```

### Run
```
# development
./bin/run.sh

# production
RAILS_ENV=production ./bin/run.sh
```

Type a name of a topic in the search field.
If it exists, select it from the list to edit.
If it doesn't exist, click create to create anew record and edit it.

## About
### Crudem
A Rails application which depends upon CrudData and CrudFace and mounts CrudFace in its routes.

### CrudData
A Rails Engine that contains a `Topic` model.

### CrudFace
A Rails Engine that provides http routes and html views, that provide a CRUD interface for CrudData.
